# Jukebox

Simple Java Spring Boot Jukebox.

## History

A long time ago I've programmed a jukebox written in C. This jukebox was
nothing special. It was just a simple console program that searched for all
music files in a directory and played them one by one. With a simple
keypress it was possible to go back and forward in the list of songs or to
stop the jukebox.   

![C jukebox](http://www.janwagemakers.be/jekyll/assets/images/2019/jukeboxC.png)

On the PC that was running this jukebox, I had connected an infrared
receiver/transmitter so I was able to control the jukebox with a remote
control and view the titles of the songs on [a little infrared
display](http://www.janwagemakers.be/pic2-e.html) that
I had build.

![IR display](http://www.janwagemakers.be/pics/IR-print.jpg)

## New jukebox

Technology has advanced. Instead of sending the song titles to a display by
infrared, it's now more logical and less cumbersome to just display it on my
smartphone. 

That's why I've rewritten my Jukebox as a Java Spring Boot program. This way I
can control my jukebox with a web browser.

![jukebox](http://www.janwagemakers.be/jekyll/assets/images/2019/jukebox.png)

I've also installed my jukebox on a Raspberry Pi now, which streams the music
with pulseaudio-dlna to my network receiver.

## Source

[https://gitlab.com/jan.wagemakers/jukebox/](https://gitlab.com/jan.wagemakers/jukebox/)

