package be.janwagemakers.jukebox;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.scheduling.annotation.Scheduled;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Order(Ordered.LOWEST_PRECEDENCE - 15)
public class Jukebox implements CommandLineRunner {
    private final Log logger = LogFactory.getLog(getClass());

    private String cmd;
    private Process p;
    private List<String> ls = new ArrayList<>();
    private int currentSongID;

    @Override
    public void run(String... args) {
        logger.info("Jukebox");

        currentSongID=0;
        cmd= "Stop";

        try {
            ls = listFilesUsingFileWalk();
        } catch (IOException e) {
            e.printStackTrace();
        }
        Collections.shuffle(ls);
    }

    @Scheduled(initialDelay = 1000, fixedDelay = 1000)
    public void repeat() {
        if (cmd.equals("Start")) {
            playSong(currentSongID);
            if(cmd.equals("Start") || cmd.equals("Forward")) {
                forward();
                cmd="Start";
            }
            if(cmd.equals("Back")) {
                back();
                cmd="Start";
            }
        } else {
            if(cmd.equals("Forward")) forward();
            if(cmd.equals("Back"))    back();
            cmd="Stop";
        }
    }

    private void forward() {
        currentSongID++;
        if (currentSongID>=ls.size()) {
            currentSongID=0;
            Collections.shuffle(ls);
        }
    }

    private void back() {
        if (currentSongID>0) currentSongID--;
    }

    private List<String> listFilesUsingFileWalk() throws IOException {
        try (Stream<Path> stream = Files.walk(Paths.get("/home/jan/jukebox/"))) {
            return stream
                    .filter(file -> !Files.isDirectory(file))
                    .map(Path::toString)
                    .collect(Collectors.toList());
        }
    }

    private void playSong(int songID) {
        try {
            // Run mpv
            p = Runtime.getRuntime().exec(new String[] { "mpv", "--quiet", ls.get(songID)});

            // Read output of mpv
            BufferedReader in = new BufferedReader(new InputStreamReader(p.getInputStream()));
            String line;
            while ((line = in.readLine()) != null) {
                System.out.println(line);
            }

            // Wait until mpv is finished
            p.waitFor();
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void setCmd(String action) {
        this.cmd = action;
        if (!action.equals("Start")) {
            if (p != null) p.destroy();
        }
    }

    public String getCmd() {
        return cmd;
    }

    private String getSong(String fileName) {
        return (new File(fileName).getName());
    }

    public String getSong() {
        return getSong(ls.get(currentSongID));
    }

    public String getSong(int notCurrent) {
        if (notCurrent==-1) {
            if (currentSongID>0) return getSong(ls.get(currentSongID -1));
            return "-";
        }
        if (currentSongID+1<ls.size()) return getSong(ls.get(currentSongID +1));
            return "-";
    }
}
