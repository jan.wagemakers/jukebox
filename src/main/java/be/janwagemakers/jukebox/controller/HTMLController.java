package be.janwagemakers.jukebox.controller;

import be.janwagemakers.jukebox.Jukebox;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

@org.springframework.stereotype.Controller
public class HTMLController {

    @Autowired private Jukebox jukebox;

    @GetMapping("/")
    public String index(Model model) {
        model.addAttribute("cmd", jukebox.getCmd());
        model.addAttribute("song", jukebox.getSong());
        model.addAttribute("previous", jukebox.getSong(-1));
        model.addAttribute("next", jukebox.getSong(1));
        return "index";
    }

    @PostMapping("/")
    public String index(@RequestParam String action, Model model ) {
        jukebox.setCmd(action);
        return index(model);
    }
}
